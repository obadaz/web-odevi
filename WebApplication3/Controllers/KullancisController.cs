﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using WebApplication3.Models;
using System.Data.SqlClient;

namespace WebApplication3.Controllers
{
    public class KullancisController : Controller
    {
      
        public ActionResult Create(FormCollection col)
        {
            //Kullanci ku = new Kullanci();
            //ku.Ad = col["Ad"];
            //ku.EPosta = col["EPota"];
            //ku.Soyad = col["Soyad"];
            //add(ku);


            foreach (string item in col)
            {
                Response.Write(item);
            }
            return View();
        }

        public void add(Kullanci k )
        {
            string cs = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
            string sql = "INSERT INTO [dbo].[Kullanci]([Ad],[Soyad],[EPosta]) VALUES('"+k.Ad+"','"+k.Soyad+"','"+k.EPosta+"')";
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.ExecuteNonQuery();
            }
        }




        private GoturkeyEntities db = new GoturkeyEntities();

        // GET: Kullancis
        public ActionResult Index()
        {
            return View(db.Kullanci.ToList());
        }

        // GET: Kullancis/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanci kullanci = db.Kullanci.Find(id);
            if (kullanci == null)
            {
                return HttpNotFound();
            }
            return View(kullanci);
        }

        // GET: Kullancis/Create
       

        // POST: Kullancis/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KullanciID,Ad,Soyad,EPosta")] Kullanci kullanci)
        {
            if (ModelState.IsValid)
            {
                db.Kullanci.Add(kullanci);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kullanci);
        }

        // GET: Kullancis/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanci kullanci = db.Kullanci.Find(id);
            if (kullanci == null)
            {
                return HttpNotFound();
            }
            return View(kullanci);
        }

        // POST: Kullancis/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KullanciID,Ad,Soyad,EPosta")] Kullanci kullanci)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kullanci).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kullanci);
        }

        // GET: Kullancis/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanci kullanci = db.Kullanci.Find(id);
            if (kullanci == null)
            {
                return HttpNotFound();
            }
            return View(kullanci);
        }

        // POST: Kullancis/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kullanci kullanci = db.Kullanci.Find(id);
            db.Kullanci.Remove(kullanci);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
